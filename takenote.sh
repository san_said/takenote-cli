#!/usr/bin/env bash

CONFPATH="/home/ubuntu/takenote-cli/.tn.conf"

takenote() {  
  . $CONFPATH
  
  # check if a note file already exists in the defined directory - if not, create one then broadcast location
  if [ ! -e "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}" ]; then
    mkdir -p "${NOTEFILEDIR}"
    touch "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
  fi 

  if (( ! "$#" )); then 
    printf "\e[93m\nWelcome to takenote! Your notefile has been saved to %s/%s%s\n\n" $NOTEFILEDIR $NOTEFILENAME $NOTEFILEEXT
    printf "\e[93mGet started by typing 'takenote \"Hello takenote!\"'\n\n"
    printf "\e[93mFor help, just type 'takenote help'\e[0m\n\n" 
  else 
    # switch and argument handling
    while (( "$#" )); do
      case "$1" in
        -d | --filedir)
          if [ -z "$2" ]; then
            echo "${NOTEFILEDIR}"
          elif [[ "$2" =~ ^\/.* ]]; then 
            echo "You are about to change your notefile directory to $2 - do you want to proceed? [y/n]"
            read res
            if [[ "$res" =~ y ]]; then
              mkdir -p "$2"
              mv "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}" "$2/${NOTEFILENAME}${NOTEFILEEXT}"
              sed -i -e "s|^NOTEFILEDIR=.*$|NOTEFILEDIR=$2|g" "${CONFPATH}"
              echo "Your new notefile directory: ${NOTEFILEDIR} - your notefile has been moved to this directory." 
            else
              echo "Your notefile directory is still ${NOTEFILEDIR}. Processing remaining arguments.."
            fi
          else
            echo "Please enter an absolute directory name for your new notefile directory."
          fi
          
          shift 2
          ;;
        -n | --filename)
          if [ -z "$2" ]; then
            echo "${NOTEFILENAME}${NOTEFILEEXT}"
          elif [ -n $2 ]; then 
            echo "You are about to change your notefile to $2${NOTEFILEEXT} - do you want to proceed? [y/n]"
            read res
            if [[ "$res" =~ y ]]; then
              mv "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}" "${NOTEFILEDIR}/$2${NOTEFILEEXT}"
              sed -i -e "s|^NOTEFILENAME=.*$|NOTEFILENAME=$2|g" "${CONFPATH}"
              echo "Your new notefile name is: ${NOTEFILENAME}${NOTEFILEEXT}" 
            else
              echo "Your notefile directory is still ${NOTEFILENAME}${NOTEFILEEXT}. Processing remaining arguments.."
            fi
          fi
          
          shift 2
          ;;
        -p | --filepath)
          echo "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
          shift
          ;;
        help | -h | --help)
          printf "takenote - a note taking CLI tool that helps quickly jot down ideas\n\n"
          printf "Options\tDescription\tExample\n\n"
          printf "[ -p | --filepath] <value>\tif no value passed in, displays the full file path of your notefile\ttakenote -p\n\n"
          shift
          ;;
        *)
          if [ "$#" -eq 1 ]; then
            if [ "$(date +%Y%m%d)" -gt "$(date -r "$NOTEFILEDIR/$NOTEFILENAME$NOTEFILEEXT" +%Y%m%d)" ] || [ ! -s "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}" ] ; then   
              echo "" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
              echo "$(date +"%d %b %Y")" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
              echo "------------" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
              echo "" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
            fi   

            echo "$1" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"
            echo "" >> "${NOTEFILEDIR}/${NOTEFILENAME}${NOTEFILEEXT}"

            shift
          else
            echo "Syntax error - please review your command or check 'takenote help' for more help."
            break
          fi
          ;;
      esac
      shift
    done
  fi     
}
