### TO-DO

- [ ] Complete writing tests for test.sh
- [ ] Decide how to package tool
- [ ] Add extensive commenting to script
- [ ] Prevent editing of files other than by the takenote tool
- [ ] Update help file
- [ ] Make conf or script file portable
- [ ] Create CI/CD pipeline for testing
